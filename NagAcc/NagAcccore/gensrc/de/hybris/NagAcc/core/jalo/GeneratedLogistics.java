/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 28-Dec-2021, 12:59:33 AM                    ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.NagAcc.core.jalo;

import de.hybris.NagAcc.core.constants.NagAccCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.product.Product;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem Logistics}.
 */
@SuppressWarnings({"deprecation","unused","cast"})
public abstract class GeneratedLogistics extends GenericItem
{
	/** Qualifier of the <code>Logistics.companyName</code> attribute **/
	public static final String COMPANYNAME = "companyName";
	/** Qualifier of the <code>Logistics.supportsBlazingFast</code> attribute **/
	public static final String SUPPORTSBLAZINGFAST = "supportsBlazingFast";
	/** Qualifier of the <code>Logistics.activeOrders</code> attribute **/
	public static final String ACTIVEORDERS = "activeOrders";
	/** Qualifier of the <code>Logistics.product</code> attribute **/
	public static final String PRODUCT = "product";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(COMPANYNAME, AttributeMode.INITIAL);
		tmp.put(SUPPORTSBLAZINGFAST, AttributeMode.INITIAL);
		tmp.put(ACTIVEORDERS, AttributeMode.INITIAL);
		tmp.put(PRODUCT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute.
	 * @return the activeOrders - number of active orders
	 */
	public Integer getActiveOrders(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, ACTIVEORDERS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute.
	 * @return the activeOrders - number of active orders
	 */
	public Integer getActiveOrders()
	{
		return getActiveOrders( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute. 
	 * @return the activeOrders - number of active orders
	 */
	public int getActiveOrdersAsPrimitive(final SessionContext ctx)
	{
		Integer value = getActiveOrders( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.activeOrders</code> attribute. 
	 * @return the activeOrders - number of active orders
	 */
	public int getActiveOrdersAsPrimitive()
	{
		return getActiveOrdersAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - number of active orders
	 */
	public void setActiveOrders(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, ACTIVEORDERS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - number of active orders
	 */
	public void setActiveOrders(final Integer value)
	{
		setActiveOrders( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - number of active orders
	 */
	public void setActiveOrders(final SessionContext ctx, final int value)
	{
		setActiveOrders( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.activeOrders</code> attribute. 
	 * @param value the activeOrders - number of active orders
	 */
	public void setActiveOrders(final int value)
	{
		setActiveOrders( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute.
	 * @return the companyName - Name of the company
	 */
	public String getCompanyName(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedLogistics.getCompanyName requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, COMPANYNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute.
	 * @return the companyName - Name of the company
	 */
	public String getCompanyName()
	{
		return getCompanyName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute. 
	 * @return the localized companyName - Name of the company
	 */
	public Map<Language,String> getAllCompanyName(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,COMPANYNAME,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.companyName</code> attribute. 
	 * @return the localized companyName - Name of the company
	 */
	public Map<Language,String> getAllCompanyName()
	{
		return getAllCompanyName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - Name of the company
	 */
	public void setCompanyName(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedLogistics.setCompanyName requires a session language", 0 );
		}
		setLocalizedProperty(ctx, COMPANYNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - Name of the company
	 */
	public void setCompanyName(final String value)
	{
		setCompanyName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - Name of the company
	 */
	public void setAllCompanyName(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,COMPANYNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.companyName</code> attribute. 
	 * @param value the companyName - Name of the company
	 */
	public void setAllCompanyName(final Map<Language,String> value)
	{
		setAllCompanyName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.product</code> attribute.
	 * @return the product - logistics of product
	 */
	public Product getProduct(final SessionContext ctx)
	{
		return (Product)getProperty( ctx, PRODUCT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.product</code> attribute.
	 * @return the product - logistics of product
	 */
	public Product getProduct()
	{
		return getProduct( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.product</code> attribute. 
	 * @param value the product - logistics of product
	 */
	public void setProduct(final SessionContext ctx, final Product value)
	{
		setProperty(ctx, PRODUCT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.product</code> attribute. 
	 * @param value the product - logistics of product
	 */
	public void setProduct(final Product value)
	{
		setProduct( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute.
	 * @return the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public Boolean isSupportsBlazingFast(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, SUPPORTSBLAZINGFAST);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute.
	 * @return the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public Boolean isSupportsBlazingFast()
	{
		return isSupportsBlazingFast( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @return the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public boolean isSupportsBlazingFastAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isSupportsBlazingFast( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @return the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public boolean isSupportsBlazingFastAsPrimitive()
	{
		return isSupportsBlazingFastAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public void setSupportsBlazingFast(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, SUPPORTSBLAZINGFAST,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public void setSupportsBlazingFast(final Boolean value)
	{
		setSupportsBlazingFast( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public void setSupportsBlazingFast(final SessionContext ctx, final boolean value)
	{
		setSupportsBlazingFast( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Logistics.supportsBlazingFast</code> attribute. 
	 * @param value the supportsBlazingFast - whether it supports blazing fast delivery or not
	 */
	public void setSupportsBlazingFast(final boolean value)
	{
		setSupportsBlazingFast( getSession().getSessionContext(), value );
	}
	
}
