<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
-->
<items xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="items.xsd">

    <collectiontypes>
        <collectiontype code="GenderList" elementtype="Gender" autocreate="true" generate="true" type="list"/>
        <collectiontype code="SwatchColorSet" elementtype="SwatchColorEnum" autocreate="true" generate="true"
                        type="set"/>
    </collectiontypes>

    <enumtypes>
        <enumtype generate="true" code="SwatchColorEnum" autocreate="true" dynamic="true">
            <value code="BLACK"/>
            <value code="BLUE"/>
            <value code="BROWN"/>
            <value code="GREEN"/>
            <value code="GREY"/>
            <value code="ORANGE"/>
            <value code="PINK"/>
            <value code="PURPLE"/>
            <value code="RED"/>
            <value code="SILVER"/>
            <value code="WHITE"/>
            <value code="YELLOW"/>
        </enumtype>
        <enumtype generate="true" code="DeliverySpeed" autocreate="true">
            <value code="BlazingFast"/>
            <value code="Express"/>
            <value code="Standard"/>
            <value code="Relaxed"/>
        </enumtype>
    </enumtypes>

    <itemtypes>

        <!-- Add your item definitions here -->
        <!--        custom model logistics-->
        <typegroup name="Logistics">
            <itemtype code="Logistics" autocreate="true" generate="true">
                <deployment table="Logistics" typecode="24002"/>
                <attributes>
                    <attribute qualifier="companyName" type="localized:java.lang.String">
                        <description>Name of the company</description>
                        <modifiers write="true" read="true" optional="false" unique="true" initial="true"/>
                        <persistence type="property"/>
                    </attribute>
                    <attribute qualifier="supportsBlazingFast" type="java.lang.Boolean">
                        <description>whether it supports blazing fast delivery or not</description>
                        <modifiers write="true" read="true" optional="true" unique="false" initial="true"/>
                        <defaultvalue>false</defaultvalue>
                        <persistence type="property"/>
                    </attribute>
                    <attribute qualifier="activeOrders" type="java.lang.Integer">
                        <description>number of active orders</description>
                        <modifiers write="true" read="true" optional="false" unique="false" initial="true"/>
                        <persistence type="property"/>
                    </attribute>
                    <attribute qualifier="product" type="Product">
                        <description>logistics of product</description>
                        <modifiers write="true" read="true" optional="false" unique="true" initial="true"/>
                        <persistence type="property"/>
                    </attribute>

                </attributes>
            </itemtype>
        </typegroup>

        <typegroup name="customProductAttribute">
            <itemtype code="Product" autocreate="false" generate="false" extends="Product">
                <attributes>
                    <attribute qualifier="deliverySpeed" type="DeliverySpeed">
                        <description>delivery type supported by the product</description>
                        <modifiers read="true" write="true" optional="true" />
                        <persistence type="property"/>
                    </attribute>
                    <attribute qualifier="logistics" type="Logistics">
                        <description>product logistics</description>
                        <modifiers read="true" write="true" optional="true" unique="true" />
                        <persistence type="property"/>
                    </attribute>

                </attributes>
            </itemtype>
        </typegroup>
        <!-- TypeGroups are for structure of this file only -->

        <typegroup name="Apparel">
            <itemtype code="ApparelProduct" extends="Product"
                      autocreate="true" generate="true"
                      jaloclass="de.hybris.NagAcc.core.jalo.ApparelProduct">
                <description>Base apparel product extension that contains additional attributes.</description>
                <attributes>
                    <attribute qualifier="genders" type="GenderList">
                        <description>List of genders that the ApparelProduct is designed for</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
            </itemtype>

            <itemtype code="ApparelStyleVariantProduct" extends="VariantProduct"
                      autocreate="true" generate="true"
                      jaloclass="de.hybris.NagAcc.core.jalo.ApparelStyleVariantProduct">
                <description>Apparel style variant type that contains additional attribute describing variant style.
                </description>
                <attributes>
                    <attribute qualifier="style" type="localized:java.lang.String"
                               metatype="VariantAttributeDescriptor">
                        <description>Color/Pattern of the product.</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>

                    <attribute qualifier="swatchColors" type="SwatchColorSet">
                        <description>A normalized color mapping to a standardized front-end navigable name.
                        </description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>

            </itemtype>

            <itemtype code="ApparelSizeVariantProduct" extends="ApparelStyleVariantProduct"
                      autocreate="true" generate="true"
                      jaloclass="de.hybris.NagAcc.core.jalo.ApparelSizeVariantProduct">
                <description>Apparel size variant type that contains additional attribute describing variant size.
                </description>
                <attributes>
                    <attribute qualifier="size" type="localized:java.lang.String"
                               metatype="VariantAttributeDescriptor">
                        <description>Size of the product.</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
            </itemtype>
        </typegroup>

        <typegroup name="Electronics">
            <itemtype code="ElectronicsColorVariantProduct" extends="VariantProduct"
                      autocreate="true" generate="true"
                      jaloclass="de.hybris.NagAcc.core.jalo.ElectronicsColorVariantProduct">
                <description>Electronics color variant type that contains additional attribute describing variant color.
                </description>
                <attributes>
                    <attribute qualifier="color" type="localized:java.lang.String"
                               metatype="VariantAttributeDescriptor">
                        <description>Color of the product.</description>
                        <modifiers/>
                        <persistence type="property"/>
                    </attribute>
                </attributes>
            </itemtype>
        </typegroup>
    </itemtypes>
</items>
